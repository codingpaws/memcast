package memcast

import (
	"sync"
)

// A subscriber to a broadcaster, created by calling NewSub()
// on a broadcaster.
type Subscriber[T any] struct {
	broadcaster *Broadcaster[T]
	ch          chan T
}

// The broadcaster, created using the NewBroadcaster func.
type Broadcaster[T any] struct {
	mutex       sync.Mutex
	subscribers []*Subscriber[T]
}

// Creates a new broadcaster to which subscribers can
// register to using the NewSub() method. By broadcasting
// a message of type T via the Broadcast(T) method, all
// subscribers will be asynchronously sent the message.
func NewBroadcaster[T any]() *Broadcaster[T] {
	return &Broadcaster[T]{
		subscribers: []*Subscriber[T]{},
	}
}

func (self *Broadcaster[T]) NewSub() *Subscriber[T] {
	self.mutex.Lock()
	defer self.mutex.Unlock()

	subscriber := &Subscriber[T]{
		ch:          make(chan T),
		broadcaster: self,
	}
	self.subscribers = append(self.subscribers, subscriber)
	return subscriber
}

// Broadcasts a message of type `T` to all non-closed subscribers.
//
// This method is non-blocking as it notifies each subscriber
// by creating a goroutine.
func (self *Broadcaster[T]) Broadcast(value T) {
	self.mutex.Lock()
	defer self.mutex.Unlock()

	for _, subscriber := range self.subscribers {
		go func(notifier chan T) {
			notifier <- value
		}(subscriber.ch)
	}
}

// Returns a channel from which subscribers can receive
// messages of type `T`.
//
// The channel will be closed when the subscriber is closed.
func (self *Subscriber[T]) Ch() <-chan T {
	return self.ch
}

// Closes the subscriber and unregisters it from the broadcaster.
//
// After calling this, the channel returned by Ch() will only
// return empty strings.
func (self *Subscriber[T]) Close() {
	self.broadcaster.mutex.Lock()
	defer self.broadcaster.mutex.Unlock()

	close(self.ch)
	subscribers := make([]*Subscriber[T], 0, len(self.broadcaster.subscribers)-1)

	for _, subscriber := range self.broadcaster.subscribers {
		if self != subscriber {
			subscribers = append(subscribers, subscriber)
		}
	}

	self.broadcaster.subscribers = subscribers
}
