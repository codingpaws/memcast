# Memcast

A simple, typed, asynchronous in-memory broadcast-subscriber system akin
to PubSub but subscribers are limited to a single broadcaster for type
safety.

## Example

```go
import "gitlab.com/codingpaws/memcast"

func main() {
	broadcaster := memcast.NewBroadcaster[string]()

	subscriber := broadcaster.NewSub()
	defer subscriber.Close()

	broadcaster.Broadcast("hello")

	value := <-subscriber.Ch()
	fmt.Println(value) // prints "hello"
}
```
