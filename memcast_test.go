package memcast_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/codingpaws/memcast"
)

const TestValue = "Lorem ipsum dolor sit amet"

func TestNewBroadcaster(t *testing.T) {
	broadcaster := memcast.NewBroadcaster[string]()
	t.Parallel()

	t.Run("broadcasting empty", func(t *testing.T) {
		broadcaster.Broadcast("hello")
	})

	t.Run("with single subscriber", func(t *testing.T) {
		subscriber := broadcaster.NewSub()
		defer subscriber.Close()

		broadcaster.Broadcast(TestValue)

		value := <-subscriber.Ch()
		require.Equal(t, TestValue, value)
	})

	t.Run("with multiple subscribers", func(t *testing.T) {
		subscriber1 := broadcaster.NewSub()
		defer subscriber1.Close()
		subscriber2 := broadcaster.NewSub()
		defer subscriber2.Close()

		broadcaster.Broadcast(TestValue)

		value1 := <-subscriber1.Ch()
		require.Equal(t, TestValue, value1)
		value2 := <-subscriber2.Ch()
		require.Equal(t, TestValue, value2)
	})

	t.Run("with multiple subscribers and multiple broadcasts", func(t *testing.T) {
		broadcaster2 := memcast.NewBroadcaster[string]()
		subscriber1 := broadcaster.NewSub()
		defer subscriber1.Close()

		subscriber2 := broadcaster2.NewSub()
		defer subscriber2.Close()

		broadcaster.Broadcast(TestValue)

		value1 := <-subscriber1.Ch()
		require.Equal(t, TestValue, value1)

		broadcaster2.Broadcast(TestValue)

		value2 := <-subscriber2.Ch()
		require.Equal(t, TestValue, value2)
	})

	t.Run("with multiple broadcasts", func(t *testing.T) {
		subscriber1 := broadcaster.NewSub()
		defer subscriber1.Close()

		broadcaster.Broadcast(TestValue)
		broadcaster.Broadcast(TestValue)

		value1 := <-subscriber1.Ch()
		require.Equal(t, TestValue, value1)
		value2 := <-subscriber1.Ch()
		require.Equal(t, TestValue, value2)
	})

	t.Run("closing subscriber closes channel", func(t *testing.T) {
		subscriber1 := broadcaster.NewSub()
		subscriber1.Close()
		broadcaster.Broadcast(TestValue)
		value := <-subscriber1.Ch()
		require.Equal(t, "", value)
	})
}

func ExampleNewBroadcaster() {
	broadcaster := memcast.NewBroadcaster[string]()

	subscriber := broadcaster.NewSub()
	defer subscriber.Close()

	broadcaster.Broadcast("hello")

	value := <-subscriber.Ch()
	fmt.Println(value)
	//Output:hello
}
